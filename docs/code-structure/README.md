Code Structure
==============

```bash
.
├── app.json
├── CODE_OF_CONDUCT.md
│      // Contributor Code of Conduct
├── CHANGELOG.md
│      // Application releasing log
├── LICENSE.md
│      // Platform license
├── LICENSE_FOR_CONTENT.md
│      // Content License
├── README.md
│      // Initial entry point for documentation
├── backend
│   ├── __init__.py
│   │     
│   ├── app.py
│   │      // Flask app definition
│   ├── commands.py
│   │      // Extra flask commands declarations
│   ├── config.py
│   │      // Database/application config
│   ├── extensions.py
│   │      // Module for declaring flask extensions (e.g. JWT)
│   ├── models
│   │      // Flask models (ORM) python classes for database mapping
│   ├── resources
│   │      // RESTful Routes
│   └── utils
│          // Custom defined utilities files
├── DCO.txt
│          // Developer Certificate of Origin
│
├── docs
│      // Documentation files
│
├── frontend
│   ├── package.json
│   │      // React dependencies list
│   ├── public
│   │   ├── index.html
│   │   │      // React application entrypoint at id="root"
│   ├── src
│   │   ├── _actions
│   │   │      // dispatch server calls and call reducers
│   │   ├── _constants
│   │   │      // Stylesheets & action types
│   │   ├── _helpers
│   │   │      // Reusable frontend helper methods
│   │   ├── _reducers
│   │   │      // State modifiers
│   │   ├── _services
│   │   │      // API calls (http requests)
│   │   ├── _store
│   │   │      // Application state tree
│   │   ├── components
│   │   │      // Core reusable components
│   │   ├── containers
│   │   │      // Main application components
│   │   ├── layouts
│   │   │      // Application layouts
│   ├── tests
│          // Frontend e2e tests
│
├── manage.py
│      // Flask app entry point
│
├── media
│      // media file storage
│
├── migrations
│      // Database migration tracking
│
├── package-lock.json
│
├── requirements.txt
│      // frozen dependencies
│
├── requirements-to-freeze.txt
│      // direct backend application dependencies list
│
├── tests
│      // Backend unit & functional tests
│
├── VagrantFile
│
└── wsgi.py
       // Production backend entrypoint

```

---
Copyright &copy; 2019 The LibreFoodPantry Community. This work is licensed
under the Creative Commons Attribution-ShareAlike 4.0 International License.
To view a copy of this license, visit
http://creativecommons.org/licenses/by-sa/4.0/.
